<?php
    require_once 'models/Cliente.php';

    $host ='127.0.0.1';
    $user = 'root';
    $password = '';

    $cliente = new Cliente();
    $controle = filter_input(INPUT_POST,'controle');
    try{
        $pdo = new PDO('mysql:host=localhost; dbname=seletivo;',$user, $password);
        // set the PDO error mode to exception
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        switch ($controle) {
            case 'inserir':
                $cliente->setNome(filter_input(INPUT_POST,'cName', FILTER_SANITIZE_SPECIAL_CHARS));
                $cliente->setCpf(filter_input(INPUT_POST,'cCpf', FILTER_SANITIZE_SPECIAL_CHARS));
                $cliente->setTelefone(filter_input(INPUT_POST,'cPhone'));
                $cliente->setEmail(filter_input(INPUT_POST,'cEmail', FILTER_VALIDATE_EMAIL));
                $cliente->setDataAniversario(filter_input(INPUT_POST,'cDataNasc'));
                $cliente->inserirCliente($pdo);
                echo json_encode($cliente->listarClientes($pdo));
                break;
            case 'listar':
                echo json_encode($cliente->listarClientes($pdo));
                break;
            case 'buscar':
                $id = filter_input(INPUT_POST,'id');
                echo json_encode($cliente->buscarCliente($id,$pdo));
                break;
            default:
                # code...
                break;
        }
        

        //Listar tudo
        
        

    }catch(PDOException $e) {
        echo "Erro de conexão: " . $e->getMessage();
    }
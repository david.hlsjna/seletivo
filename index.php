<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Seletivo Programador</title>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        
        <style>
            .container{
                margin-top: 2%;
            }
        </style>
    </head>
    <body onload="view_data()"> 
        <!-- NAV BAR-->
        <nav class="navbar navbar-dark bg-dark">
            <!-- Navbar content -->
            <h3 class="navbar-text">
                Seletivo Programador PHP 
            </h3>
        </nav>
        <div class="container">
            <!-- FORM-->
            <div class="row">
                <div class="col-md-6">
                    <h2>
                        Clientes
                    </h2>
                </div>
                <div class="col-md-2">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#form-modal">
                    Cadastrar
                    </button>
                </div>
                <div class="col-md-2">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#buscar-modal">
                    Buscar
                    </button>
                </div>
            </div>
            
            <div class="row" style="margin-top: 5%">
                <div class="col-md-12">
                    <table class="table  table-hover" id="tbListar">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Telefone</th>
                                <th scope="col">Email</th>
                                <th scope="col">Aniversário</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
        <!--modal inserir-->
        <div class="modal fade" id="form-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>Cadastrar</h3>
                        <button type="button" class="close btn btn-primary btn-lg" data-dismiss="modal">
                            <span>&times</span>
                        <buutton>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class='col-md-12 form-crud'>
                                    <div class='form'>
                                        <form action='' method ='post' id='form-send'>
                                            <div class='form-group'>
                                                <label for="tNome" id='nome'>Digite o nome completo:</label>
                                                <input id='tNome' name='cName' class='form-control' required>
                                            </div>
                                            <div class="form-group">
                                                <label for="tCpf">CPF:</label>
                                                <input type="text" class="form-control" id="tCpf" name='cCpf' required>
                                            </div>
                                            <div class="form-group">
                                                <label for="tEmail">Email</label>
                                                <input type="email" class="form-control" id="tEmail" name='cEmail' required>
                                            </div>
                                            <div class="form-group">
                                                <label for="tPhone">Telefone:</label>
                                                <input class="form-control" id="tPhone" name='cPhone' required>
                                            </div>
                                            <div class="form-group">
                                                <label for="tDataNasc">Data de Nascimento:</label>
                                                <input type='text' class="form-control" id="tDataNasc" name='cDataNasc' required>
                                            </div>
                                        </form>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" form ="form-send" id="send" class="btn btn-success btn-lg col-md-6 cSend">Salvar</button>
                        <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- MODAL REMOVER-->

        <div class="modal fade" id="buscar-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>Buscar</h3>
                        <button type="button" class="close btn btn-primary btn-lg" data-dismiss="modal">
                            <span>&times</span>
                        <buutton>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class='col-md-12 form-crud'>
                                    <div class='form-searh'>
                                        <form action='' method ='post' id='form-search'>
                                            <div class='form-group'>
                                                <label for="tId" id='id'>Digite o ID que deseja procurar:</label>
                                                <input id='tId' type="number" name='cId' class='form-control' required>
                                            </div>
                                        </form>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" form ="form-search" id="send" class="btn btn-primary btn-lg col-md-6 cSend">Buscar</button>
                        <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>    
        <script src="scripts/js/jquery.mask.js"></script>
        <script src="scripts/js/jquery.tabledit.js"></script>
        <script src="scripts/scripts.js"></script>
        
    </body>
</html>
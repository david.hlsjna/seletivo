<?php
    require_once 'models/Produto.php';

    $host ='127.0.0.1';
    $user = 'root';
    $password = '';

    $produto = new Produto();
    $controle = filter_input(INPUT_POST,'controle');

    try{
        $pdo = new PDO('mysql:host=localhost; dbname=seletivo;',$user,$password);
        // set the PDO error mode to exception
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo json_encode($controle);
        switch ($controle) {
            case 'inserir':
                $produto->setNome(filter_input(INPUT_POST,'cName', FILTER_SANITIZE_SPECIAL_CHARS));
                $produto->setPreco(filter_input(INPUT_POST,'cPreco', FILTER_SANITIZE_SPECIAL_CHARS));
                $produto->setDescricao(filter_input(INPUT_POST,'cDescricao', FILTER_SANITIZE_SPECIAL_CHARS));
                $produto->setCodigo(filter_input(INPUT_POST,'cCodigo'));
                $produto->setQuantidade(filter_input(INPUT_POST,'cQuantidade', FILTER_VALIDATE_EMAIL));
               
                $produto->inserirProduto($pdo);
                echo json_encode($produto->getNome());
                break;
            case 'listar':
                echo json_encode($produto->listarProdutos($pdo));
                break;
            case 'buscar':
                $id = filter_input(INPUT_POST,'id');
                //echo json_encode($cliente->buscarCliente($id,$pdo));
                break;
            default:
                # code...
                break;
        }
        //Listar tudo
        //echo json_encode($cliente->listarClientes($pdo));
        

    }catch(PDOException $e) {
        echo "Erro de conexão: " . $e->getMessage();
    }
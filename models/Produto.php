<?php
    class Produto{
        private $nome;
        private $codigo;
        private $preco;
        private $descricao;
        private $quantidade;

        public function __construct (){}

        public function getNome(){
            return $this->nome;
        }
        public function getCodigo(){
            return $this->codigo;
        }
        public function getPreco(){
            return $this->preco;
        }
        public function getDescricao(){
            return $this->descricao;
        }
        public function getQuantidade(){
            return $this->quantidade;
        }

        public function setNome($nome){
            $this->nome = $nome;
        }
        public function setCodigo($codigo){
            $this->codigo = $codigo;
        }

        public function setPreco($preco){
            $this->preco = $preco;
        }

        public function setDescricao($descricao){
            $this->descricao = $descricao;
        }
        public function setQuantidade($quantidade){
            $this->quantidade = $quantidade;
        }
        //Funcoes do BD
        public function inserirProduto($pdo){
            
            $sql = "INSERT INTO produtos (codigo, nome, preco , descricao , quantidade) values ('$this->nome','$this->codigo','$this->preco','$this->descricao','$this->quantidade')";
            $insert = $pdo->prepare($sql);
            $insert->execute();
            $id = $pdo->lastInsertId();            

        }
        public function listarProdutos($pdo){
            $sql = "SELECT * FROM produtos";
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            
            if($stmt->rowCount() >=1 ){
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return "Deu erro";
            }
            
        }
        public function buscarProduto($id, $pdo){
            $sql = "SELECT * FROM produtos WHERE id=$id";
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            if($stmt->rowCount() >=1 ){
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return "Deu erro";
            }
        }

        public function editarProduto($pdo, $sql){
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
        }

        public function removerProduto($id, $pdo){
            $sql = "DELETE FROM usuario WHERE id=$id";
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
        }

    }
<?php
    //nome, 
    class Cliente{
        private $nome;
        private $cpf;
        private $email;
        private $telefone;
        private $data_aniversario;

        public function __construct (){}

        public function getNome(){
            return $this->nome;
        }
        public function getCpf(){
            return $this->cpf;
        }
        public function getEmail(){
            return $this->email;
        }
        public function getTelefone(){
            return $this->telefone;
        }
        public function getDataAniversario(){
            return $this->data_aniversario;
        }

        public function setNome($nome){
            $this->nome = $nome;
        }
        public function setCpf($cpf){
            $this->cpf = $cpf;
        }

        public function setEmail($email){
            $this->email = $email;
        }

        public function setTelefone($telefone){
            $this->telefone = $telefone;
        }
        public function setDataAniversario($data_aniversario){
            $this->data_aniversario = $data_aniversario;
        }
        
        public function inserirCliente($pdo){
            $sql = "INSERT into usuario (nome,cpf,email,telefone,dataAniversario) values ('$this->nome','$this->cpf','$this->email','$this->telefone','$this->data_aniversario')";
            $insertUser = $pdo->prepare($sql);
            $insertUser->execute();
            $idUser = $pdo->lastInsertId();            

        }
        public function listarClientes($pdo){
            $sql = "SELECT * FROM usuario";
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            if($stmt->rowCount() >=1 ){
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return "Deu erro";
            }
            
        }
        public function buscarCliente($id_cliente, $pdo){
            $sql = "SELECT * FROM usuario WHERE id_user=$id_cliente";
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            if($stmt->rowCount() >=1 ){
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return "Deu erro";
            }
        }

        public function editarCliente($pdo, $sql){
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
        }

        public function removerCliente($id_cliente, $pdo){
            $sql = "DELETE FROM usuario WHERE id_user=$id_cliente";
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
        }
        
        
        

    }
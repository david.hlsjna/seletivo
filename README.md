# Seletivo

É necessário importar o banco de dados através do arquivo ".sql".
Para rodar o código e usar a interface para cadastro de Cliente inicie um servidor local e acesse o link "localhost/seletivo".
Para cadastrar produtos utilize o link "localhost/seletivo/view-produtos";

Tanto a tela de cliente quanto a de produtos, possuem contem o botão para cadastrar e a tabela com os dados.
Ao clicar em "cadastrar" vai abrir um modal com o formulário, ao salvar insere no banco, gera um alerta de sucesso e volta para o modal, permitindo que o usuário continue inserindo, para fechar o modal basta clicar no "x" ou no botão "cancelar".
A tabela possui os dados e ao lado dois botões, um para editar e outro para remover. O de editar, muda as tuplas da tabela para receber a informação nova, para concluir basta apertar "save" que a informação é atualizada e exibe um alerta. Para cancelar a operção basta apertar novamente no botão ou a tecla "esc". O botão de remover pede confirmação e se der sequência um alerta é exibido mostrando que a operção foi bem sucedida.



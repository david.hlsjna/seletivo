<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Seletivo Programador</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <style>
            .container{
                margin-top: 2%;
            }
        </style>
    </head>
    <body onload="view_data()"> 
        <!-- NAV BAR-->
        <nav class="navbar navbar-dark bg-dark">
            <!-- Navbar content -->
            <h3 class="navbar-text">
                Seletivo Programador PHP 
            </h3>
        </nav>


        <div class="container">
            <!-- FORM-->
            <div class="row">
                <div class="col-md-10">
                    <h2>
                        Produtos
                    </h2>
                </div>
                <div class="col-md-2">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#form-modal">
                    Cadastrar
                    </button>
                </div>
            </div>

            <div class="row" style="margin-top: 5%">
                <div class="col-md-12">
                    <table class="table  table-hover" id="tbListar-produto">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Código</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Preço</th>
                                <th scope="col">Descrição</th>
                                <th scope="col">Quantidade</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>     
            <!-- TABELA-->
        </div>


        <div class="modal fade" id="form-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3>Cadastrar</h3>
                        <button type="button" class="close btn btn-primary btn-lg" data-dismiss="modal">
                            <span>&times</span>
                        <buutton>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12" id="form-ocult">
                                <form action='' method ='post' id='form-send'>
                                        <div class='form-group'>
                                            <label for="tNome" id='nome'>Nome do produto:</label>
                                            <input id='tNome' name='cName' class='form-control' required>
                                        </div>
                                        <div class="form-group">
                                            <label for="tCodSku">Código (SKU):</label>
                                            <input type="int" class="form-control" id="tCodSku" name='cCodSku' required>
                                        </div>
                                        <div class="form-group">
                                            <label for="tPrice">Preço</label>
                                            <input type="text" class="form-control" id="tPrice" name='cPrice' required>
                                        </div>

                                        <div class="form-group">
                                            <label for="tDescription">Descrição:</label>
                                            <input class="form-control" id="tDescription" name='cDescription' required>
                                        </div>
                                        <div class="form-group">
                                            <label for="tDataNasc">Quantidade:</label>
                                            <input type='int' class="form-control" id="tQuant" name='cQuant' required>
                                        </div>
                                        
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" form ="form-send" id="send" class="btn btn-success btn-lg col-md-6 cSend">Salvar</button>
                        <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="scripts/js/jquery.tabledit.js"></script>
        <script src="scripts/scripts-produto.js"></script>
        
    </body>
</html>
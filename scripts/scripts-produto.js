function view_data(){
    $("#tbListar-produto tbody").html("");
    var token = $("input[name*='_token']").val();
    $.ajax({
        url: "process_produto.php",
        data: {
            controle: 'listar',
            _token: token,
        },
    }).done(function(e){
        if(e.length>0){
            for(var i=0; i<e.length; i++){
                var html = '<tr><th scope="row">'+e[i].codigo+'</th><td>'+e[i].nome+'</td><td>'+e[i].preco+'</td><td>'+e[i].descricao+'</td><td>'+e[i].quantidade+'</td></tr>';
                $('#tbListar-produto tbody').append(html);
            }
        }else{
            console.log('Não possui dados');
        }
        tableData();
    });
    
}
function tableData(){
    $('#tbListar-produto').Tabledit({
        url: 'action-produto.php',
        eventType: 'dblclick',
        editButton: true,
        deleteButton: true,
        columns: {
            identifier: [0,'Codigo'],
            editable: [[1, 'Nome'], [2, 'Preco'], [3, 'Descricao'], [4,'Quantidade']]
        },
        onSuccess: function(data, textStatus, jqXHR) {
            view_data();
            if(data.action == 'delete'){
                alert(data.nome + "Deletado com sucesso!");
            }else if(data.action == 'edit'){
                alert(data.nome + "Alterado com sucesso!");
            }
            console.log('onSuccess(data, textStatus, jqXHR)');
            
        },
        onFail: function(jqXHR, textStatus, errorThrown) {
            console.log('onFail(jqXHR, textStatus, errorThrown)');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        },

    });
}

$(document).ready(function(){
    
    $('#tCancel').click(function () {
        location.reload();
    });

    //AJAX
    $('#form-send').submit(function(e){
        e.preventDefault();
        
        //Validacao das variaveis
        var cNome = $('#tNome').val();
        var cCodigo = $('#tCodSku').val();
        var cPreco = $('#tPrice').val();
        var cDescricao = $('#tDescription').val();
        var cQuantidade = $('#tQuant').val();

        var token = $("input[name*='_token']").val();

       
        $.ajax({
            url: "process_produto.php",
            method: "POST",
            dataType: "JSON",
            data: {
                cName: cNome,
                cPreco: cPreco,
                cCodigo: cCodigo,
                cDescricao: cDescricao,
                cQuantidade: cQuantidade,
                _token: token,
            },
            cache: false,
            success: function (e){ 
                console.log(e);
            },
            error: function (e) {
                console.log(e);
            },
        });
    });
});



//fução necessária para adaptar o dado na hora de enviar para o BD
function convert_date(dateIn){
    var vetD;
    //Condicional para verificar o formato da data dd-mm-yy ou dd/mm/yy e faz a conversão de um para o outro
    //Ou seja, a função converte tanto para armazenar quanto para exibir no formato correto
    if(dateIn.includes('-')){
        vetD = dateIn.split('-');
        dateIn = vetD[2]+'/'+vetD[1]+'/'+vetD[0];
    }else if(dateIn.includes('/')){
        vetD = dateIn.split('/');
        dateIn = vetD[2]+'-'+vetD[1]+'-'+vetD[0];
    }
    return dateIn;

}

function view_data(){
    $("#tbListar tbody").html("");
    var token = $("input[name*='_token']").val();
    $.ajax({
        url: "process.php",
        method: "POST",
        dataType: "JSON",
        data: {
            controle: 'listar',
            _token: token,
        },
    }).done(function(e){
        if(e.length>0){
            for(var i=0; i<e.length; i++){
                var html = '<tr><th scope="row">'+e[i].id_user+'</th><td>'+e[i].nome+'</td><td>'+e[i].telefone+'</td><td>'+e[i].email+'</td><td>'+e[i].dataAniversario+'</td></tr>';
                $('#tbListar tbody').append(html);
            }
        }else{
            console.log('Não possui dados');
        }
        tableData();
    });
    
}
function tableData(){
    $('#tbListar').Tabledit({
        url: 'action.php',
        eventType: 'dblclick',
        editButton: true,
        deleteButton: true,
        columns: {
            identifier: [0,'Id'],
            editable: [[1, 'Nome'], [2, 'Telefone'], [3, 'Email'], [4,'Aniversario']]
        },
        onSuccess: function(data, textStatus, jqXHR) {
            view_data();
            if(data.action == 'delete'){
                alert(data.nome + "Deletado com sucesso!");
            }else if(data.action == 'edit'){
                alert(data.nome + "Alterado com sucesso!");
            }
            console.log('onSuccess(data, textStatus, jqXHR)');
            
        },
        onFail: function(jqXHR, textStatus, errorThrown) {
            console.log('onFail(jqXHR, textStatus, errorThrown)');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        },
        onAlways: function() {
            console.log('onAlways()');
        },
        

    });
}

$(document).ready(function(){
    var controle = "listar";

    $('#tCancel').click(function () {
        location.reload();
    });

    $('#tPhone').mask('(00) 0 0000-0000');
    $('#tDataNasc').mask('00/00/0000');
    $('#tCpf').mask('000.000.000-00');
    
    $('#Telefone').mask('(00) 0 0000-0000');
    $('#Email').mask('00/00/0000');
    $('#Aniversario').mask('000.000.000-00');
    
    
    //AJAX para inserir
    $('#form-send').submit(function(e){
        e.preventDefault();
        
        //Validacao das variaveis
        var cName = $('#tNome').val();
        var cPhone = $('#tPhone').val();
        var cCpf = $('#tCpf').val();
        var cEmail = $('#tEmail').val();
        var cDataNasc = $('#tDataNasc').val();
        var token = $("input[name*='_token']").val();
        
        cDataNasc = convert_date(cDataNasc);
        controle = "inserir";
        
        $.ajax({
            url: "process.php",
            method: "POST",
            dataType: "JSON",
            data: {
                cName: cName,
                cPhone: cPhone,
                cCpf: cCpf,
                cEmail: cEmail,
                cDataNasc: cDataNasc,
                controle: controle,
                _token: token,
            },
            cache: false,
            success: function (e){ 
                $('#tNome').val('');
                $('#tPhone').val('');
                $('#tCpf').val('');
                $('#tEmail').val('');
                $('#tDataNasc').val('');
                
                
                alert("Cliente inserido com sucesso."); 
                
                view_data();
                controle="listar";
            },
            error: function (e) {
                console.log(e);
            },
        });
    });


    //Ajax para buscar
    $('#form-search').submit(function(e){
        e.preventDefault();
        
        //Validacao das variaveis
        var id = $("#tId").val();
        var token = $("input[name*='_token']").val();
        controle = "buscar";
        
        $.ajax({
            url: "process.php",
            method: "POST",
            dataType: "JSON",
            data: {
                id: id,
                controle: controle,
                _token: token,
            },
            cache: false,
            success: function (e){ 
                $('#tId').val('');
                $("#tbListar tbody").html("");
                alert("Usuário "+e[0].nome+" foi encontrado!");
                view_data();
                controle = "listar";
            },
            error: function (e) {
                console.log(e);
            },
        });
    });

});
